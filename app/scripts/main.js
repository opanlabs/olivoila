$( document ).ready(function() {

    $('.multiple-items').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay:true,
        arrows:false
    });

    $('a[href^="#"]').bind('click.smoothscroll',function (e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
    
        $('html, body').stop().animate( {
          'scrollTop': $target.offset().top-0
        }, 900, 'swing', function () {
          window.location.hash = target;
        } );
    });

    new WOW().init();

    // $(".nav-pills li a").click(function(){
    //   $(".nav-pills li").removeClass("active");
    //   $(this).parent().addClass("active");
    // });
    

    var container = $('.card-body').jScrollPane({
      showArrows: false,
      autoReinitialise: true
    });
    var jsp = container.data('jsp');

    $(window).on('resize', function(){
        jsp.reinitialise();
    });

    $( '.recipe-block' ).click(function() {
      $( '.detail-recipe' ).show();
    });

    $('.close').click(function() {
      $('.detail-recipe').hide();
    });

    $('.buy').click(function() {
      $('.floating-block').toggle();
    });
    
});

$(function(){ 
  var navMain = $('.navbar-collapse');

  navMain.on('click', 'a', null, function () {
      navMain.collapse('hide');
  });
});